import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Bucket } from '../model/IBucket';
@Injectable({
  providedIn: 'root'
})
export class BucketService {

  constructor(private http: HttpClient) { }

  getBuckets() {
    //get query here
    return this.http.get('health/buckets')
      .toPromise()
      .then(data => { return <Bucket[]>data; })
      .catch((err) => { console.log(err); });
  }
}
