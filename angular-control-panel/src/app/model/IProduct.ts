export interface Product {
    id: string;
    name: string;
    description: string;
    count: number;
    calories: number;
    price: number;
    imgUrl: string;
}