import {Product} from './IProduct';

export interface Bucket {
    id: number;
    name: string;
    ip: string;
    items?: Product[];
    status: boolean;

}