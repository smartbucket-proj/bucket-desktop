import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HTTPInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const apiReq = req.clone({ 
      url: `http://localhost:5001/${req.url}`,
      // setHeaders: {
      //   ...req.headers,
      //   'Access-Control-Allow-Origin': '*'
      // }
    });

    return next.handle(apiReq);
  }
}
