import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { Bucket } from './model/IBucket';

import { BucketService } from './services/bucket-service.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-control-panel';
  items: MenuItem[];
  buckets: Bucket[];
  loading: boolean = true;

  constructor(private bucketService: BucketService) {
    this.items = Array<MenuItem>();
    this.buckets = Array<Bucket>();
  }

  ngOnInit() {
    this.items = [
      {
        label: 'Провека оборудования',
        icon: 'pi pi-fw pi-refresh',
        command: (e) => {
            this.bucketService.getBuckets().then((data) => { 
              this.buckets = <Bucket[]>data;
              console.log(this.buckets);
            });
        }
      },
      {
        label: 'Карта магазина',
        icon: 'pi pi-compass',
        command: (e) => {

        }
      }
    ];
  }
}
